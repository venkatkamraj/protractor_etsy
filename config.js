
exports.config = {
	framework: 'jasmine',
	seleniumAddress: 'http://localhost:4444/wd/hub',
	//specs: ['spec.js', 'cart.spec.js'],
	specs: ['./pages/*/*.spec.js'],
	params: {
		url: 'https://www.etsy.com'
	},
	suites: {
		cart: 'pages/cart/**/*.spec.js',
		home: 'pages/home/**/*.spec.js',
		search: 'pages/search/**/*.spec.js'
	},
	onPrepare: function(){
		browser.ignoreSynchronization = true;
		 browser.driver.manage().window().maximize();
	},
	//browserName: 'chrome',
	//directConnect: true,

	multiCapabilities: [
	{
		'browserName': 'chrome'
	/*},
	{	
		'browserName' : 'firefox'
*/
	}

	]

}