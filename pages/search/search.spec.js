var helper = require('../../helper');
    SearchPage = require('./search.po.js');

var searchPage = new SearchPage();

describe('Cart Page : ',  function (){

	beforeEach(function() {
        browser.get(browser.params.url);
    });

    afterEach(function() {
        browser.manage().deleteAllCookies();
    });



it('Search Page', function (done) {
	
	
	searchPage.searchField.sendKeys('vinyl');
	.then(function(){
		return helper.waitUntilReady(searchPage.searchButton)
	})
	.then (function(){
		return search.searchButton.click();
	})
	.then (function () {
		return helper.waitUntilReady(searchPage.searchItem)
	})
	.then (function (){
		return searchPage.searchItem.click();
	})
	.then(done);

	//helper.waitUntilReady(searchPage.addToCart);
	//searchPage.addToCart.click();
	//expect(sizeErrorMsg.getText()).toBe('Please select a size');

	});
});