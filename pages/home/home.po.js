var helper = require('../../helper');
var faker = require('faker');

var HomePage = function() {

      var fakerFname = faker.name.firstName();
      var fakerLname = faker.name.lastName();
      var email =faker.internet.email();
      var password = faker.internet.password();
      var userName = faker.internet.password();

	this.title = 'Etsy.com | Shop for anything from creative people everywhere';
	this.passwordExistsText = 'Password was incorrect';


	this.signInButton = element(by.css('#sign-in'));  
  	this.uName = element(by.css('#username-existing'));  
  	this.pwd = element(by.css('#password-existing'));
  	this.logIn = element(by.css('#signin-button'));  
  	this.pwdErrorText = element(by.css('#password-existing-error'));

  	// registration
  	this.registerButton = element(by.css ('#register'));
  	this.fName = element(by.css('#first-name'));
  	this.lName = element(by.css('#last-name'));
  	this.eMail = element (by.css('#email'));
  	this.pwdField = element (by.css('#password'));
  	this.pwdConfirm = element (by.css('#password-repeat'));
  	this.uName = element (by.css('#username'));
  	this.etsyFinds = element(by.css('#etsy_finds'));
  	this.FormRegisterButton = element(by.css('#register_button'));

  	this.goToRegister = function() {

	this.registerButton.click();

  	}
  	this.doRegister = function(){
  		helper.waitUntilReady(this.fName);
    	this.fName.sendKeys (fakerFname);
    	this.fName.sendKeys (fakerLname);
    	this.eMail.sendKeys (email);
    	this.pwdField.sendKeys(password);
    	this.pwdConfirm.sendKeys(password)
    	this.uName.sendKeys(userName)
  	    this.etsyFinds.click();
  	}
}
module.exports = HomePage;