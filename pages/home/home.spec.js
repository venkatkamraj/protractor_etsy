  var helper = require('../../helper');
  var faker = require('faker');
  var HomePage = require('./home.po.js');


  describe('Protractor Demo App', function() {
    
      beforeEach(function() {
          browser.get(browser.params.url);
          //browser.manage().window().maximize();
          browser.sleep(1000);
      });

      afterEach(function() {
          browser.manage().deleteAllCookies();
      });

      var homePage = new HomePage();

    it('should have a title', function(){
    	expect(browser.getTitle()).toEqual(homePage.title);
    });

    it('Should be signin', function(){
      
      browser.sleep(1000);
      homePage.signInButton.click();
    	helper.waitUntilReady(homePage.uName);
    	homePage.uName.sendKeys('test');
    	homePage.pwd.sendKeys('test');
    	homePage.logIn.click();
    	helper.waitUntilReady(homePage.pwdErrorText);
    	expect(homePage.pwdErrorText.getText()).toBe(homePage.passwordExistsText);
    });

    it('Register', function(){

    	homePage.goToRegister();
      homePage.doRegister();
    	
    	});

  });