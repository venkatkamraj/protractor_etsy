var CartPage = require('./cart.po.js');
var CommonElements = require('../common/common.po.js');

var commonElements = new CommonElements();
var cartPage = new CartPage();

describe('Cart Page : ',  function (){

	beforeEach(function() {
        browser.get(browser.params.url);
    });

    afterEach(function() {
        browser.manage().deleteAllCookies();
    });



it('Should navigate to empty cart message', function () {
	
	commonElements.cartIcon.click();
	

	expect(cartPage.cartErrorMsg.getText()).toBe(cartPage.emptyCartNotification);
	});
});