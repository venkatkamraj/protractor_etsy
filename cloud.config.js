require('dotenv').config({
	silent: true
});

module.exports.comfig={
	seleniumAddress: 'http://hub.browserstack.com/wd.hub',
	framework: 'jasmine',
	specs:[

		'.pages/**/*.spec.jasmine'

	],
	params: {
		url:'https://www.abc.com',
		login: {
			user: process.env.ETST_USER,
			password: process.env.ETST_PASSWORD
		}
	}

}